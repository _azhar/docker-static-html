$(function(){
	//hover over each square container with the titles and add the animate.css class
	$('.square-container').on('mouseenter', function(){
		$(this).children('.circle-text').addClass('animated fadeOutLeft');
	});

	//hover out of the square container, remove the class
	$('.square-container').on('mouseleave', function(){
		$(this).children('.circle-text').removeClass('animated fadeOutLeft');
	});
});